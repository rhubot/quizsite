# README #

This code allows you to create your own quiz that is administered through a web browser. There are several online quiz sites out there, but they all had one or two features that were missing or that I didn't like. To remedy this, I decided to create my own quiz.

### History ###

The quiz was originally written as a Java Applet. That idea was thrown out the window when people constantly complained about the need to install Java. It was re-written in JavaScript, which is what you can find in this repo.

### How do I get set up? ###

Three files are needed to create a quiz.

1. HTML file - this file is the main page where you can give the user instructions about the quiz they are about to take. This should be customized to the specific quiz you wish to make.

1. Questions file - this is a JavaScript file which contains all the questions for your quiz. The file name scheme is <your quiz name>_q-<date>.js, although this will change in the future to remove the date from the filename.

1. Quiz file - this is the main logic for the quiz site. It is currently fragmented in that you need a different file for different types of quiz questions (multiple choice vs fill-in), but this will eventually merge to become a single JavaScript file. The file name scheme is <your quiz name>_quiz.js

Once you have edited the three files to meet your needs, you can open the HTML file in a web browser to take the quiz. This can be done locally or it can be uploaded to a web hosting service.

### Who do I talk to? ###

* Repo owner or admin
* Submit a bug/feature request through Bitbucket