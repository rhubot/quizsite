var quiz = new Array();
var allQuestionsPool = new Array();
var currentQuestionNum = 0;
var score = 0;

var currentQuestion = new Array();
var prevQuestion = new Array();

var answeredQuestions = new Array();
var answeredQuestionsAnswers = new Array();
var answeredQuestionsCorrectAns = new Array();
var answeredQuestionsCitations = new Array();

var section1numQs = 0;
var section1numQCorrect = 0;
var section2numQs = 0;
var section2numQCorrect = 0;
var section3numQs = 0;
var section3numQCorrect = 0;
var section4numQs = 0;
var section4numQCorrect = 0;
var section5numQs = 0;
var section5numQCorrect = 0;
var section6numQs = 0;
var section6numQCorrect = 0;
var section7numQs = 0;
var section7numQCorrect = 0;
var section8numQs = 0;
var section8numQCorrect = 0;
var section9numQs = 0;
var section9numQCorrect = 0;
var section10numQs = 0;
var section10numQCorrect = 0;

var correctAnswer = 0;
var r = 0;
var quizSize = 0;
var totalNumQuestions;

allQuestionsPool[r++] = ('autumn~aki~1');
allQuestionsPool[r++] = ('rain~ame~1');
allQuestionsPool[r++] = ('tomorrow~ashita~1');
allQuestionsPool[r++] = ('chair~isu~1');
allQuestionsPool[r++] = ('arm~ude~1');
allQuestionsPool[r++] = ('home~uchi~1');
allQuestionsPool[r++] = ('smiling face~egao~1');
allQuestionsPool[r++] = ('train station~eki~1');
allQuestionsPool[r++] = ('music~ongaku~1');

allQuestionsPool[r++] = ('stomach~onaka~2');
allQuestionsPool[r++] = ('umbrella~kasa~2');
allQuestionsPool[r++] = ('corporation/office~kaisha~2');
allQuestionsPool[r++] = ('north~kita~2');
allQuestionsPool[r++] = ('cloud~kumo~2');
allQuestionsPool[r++] = ('shoes~kutsu~2');
allQuestionsPool[r++] = ('airport~kuukou~2');
allQuestionsPool[r++] = ('fight/argument~kenka~2');
allQuestionsPool[r++] = ('cell phone~keitai~2');

allQuestionsPool[r++] = ('marriage~kekkon~3');
allQuestionsPool[r++] = ('park~kouen~3');
allQuestionsPool[r++] = ('lower back~koshi~3');
allQuestionsPool[r++] = ('fish~sakana~3');
allQuestionsPool[r++] = ('Japanese rice wine~sake~3');
allQuestionsPool[r++] = ('wallet~saifu~3');
allQuestionsPool[r++] = ('job,work~shigoto~3');
allQuestionsPool[r++] = ('April~shigatsu~3');

allQuestionsPool[r++] = ('back~senaka~4');
allQuestionsPool[r++] = ('explanation~setsumei~4');
allQuestionsPool[r++] = ('teacher~sensei~4');
allQuestionsPool[r++] = ('sky~sora~4');
allQuestionsPool[r++] = ('cleaning~souji~4');
allQuestionsPool[r++] = ('sun~taiyou~4');
allQuestionsPool[r++] = ('waterfall~taki~4');
allQuestionsPool[r++] = ('birthday~tanjoubi~4');

allQuestionsPool[r++] = ('map~chizu~5');
allQuestionsPool[r++] = ('earth~chikyuu~5');
allQuestionsPool[r++] = ('moon~tsuki~5');
allQuestionsPool[r++] = ('next(turn)~tsugi~5');
allQuestionsPool[r++] = ('planner~techou~5');
allQuestionsPool[r++] = ('gloves~tebukuro~5');
allQuestionsPool[r++] = ('watch/clock~tokei~5');
allQuestionsPool[r++] = ('friend~tomodachi~5');

allQuestionsPool[r++] = ('bird~tori~6');
allQuestionsPool[r++] = ('summer~natsu~6');
allQuestionsPool[r++] = ('west~nishi~6');
allQuestionsPool[r++] = ('baggage~nimotsu~6');
allQuestionsPool[r++] = ('cat~neko~6');
allQuestionsPool[r++] = ('fever~netsu~6');
allQuestionsPool[r++] = ('remainder~nokori~6');
allQuestionsPool[r++] = ('throat~nodo~6');

allQuestionsPool[r++] = ('fireworks~hanabi~7');
allQuestionsPool[r++] = ('spring~haru~7');
allQuestionsPool[r++] = ('left~hidari~7');
allQuestionsPool[r++] = ('airplane~hikouki~7');
allQuestionsPool[r++] = ('east~higashi~7');
allQuestionsPool[r++] = ('winter~fuyu~7');
allQuestionsPool[r++] = ('bag/pouch~fukuro~7');
allQuestionsPool[r++] = ('reply/response~henji~7');

allQuestionsPool[r++] = ('bloom~houki~8');
allQuestionsPool[r++] = ('star~hoshi~8');
allQuestionsPool[r++] = ('pillow~makura~8');
allQuestionsPool[r++] = ('everyday~moinichi~8');
allQuestionsPool[r++] = ('south~minami~8');
allQuestionsPool[r++] = ('right(direction)~migi~8');
allQuestionsPool[r++] = ('tangerine~mikan~8');
allQuestionsPool[r++] = ('son~musuko~8');

allQuestionsPool[r++] = ('business card~meishi~9');
allQuestionsPool[r++] = ('eyeglasses~megane~9');
allQuestionsPool[r++] = ('peach~momo~9');
allQuestionsPool[r++] = ('blanket~moufu~9');
allQuestionsPool[r++] = ('vegetable~yasai~9');
allQuestionsPool[r++] = ('rest,break~yasumi~9');
allQuestionsPool[r++] = ('snow~yuki~9');
allQuestionsPool[r++] = ('finger~yubi~9');

allQuestionsPool[r++] = ('dream~yume~10');
allQuestionsPool[r++] = ('night~yoru~10');
allQuestionsPool[r++] = ('chore/errand~youji~10');
allQuestionsPool[r++] = ('next week~raishuu~10');
allQuestionsPool[r++] = ('apple~ringo~10');
allQuestionsPool[r++] = ('lotus root~rinkon~10');
allQuestionsPool[r++] = ('practice~rinshuu~10');
allQuestionsPool[r++] = ('Japanese food~washoku~10');


allQuestionsPool[r++] = ('BLANK~BLANK~1');

totalNumQuestions = r - 1;
