﻿/**
 * Check the user's answer for the current question and load the next question.
 * 
 * @param theForm
 */
function checkTextAnswer(theForm) {
	var userAnswer = theForm.elements.givenAnswer.value;

	// Clear previous question
	prevQuestion.splice(0, prevQuestion.length);
	
	// Copy current question into previous question
	for(var i=0; i<currentQuestion.length; i++){
		prevQuestion[i] = currentQuestion[i];
	}

	// Display the previous question info
	document.getElementById("pq").innerHTML='Previous Question: '+prevQuestion[0];
	document.getElementById("pa1").innerHTML='You answered: '+userAnswer;
	document.getElementById("pc").innerHTML='Citation/section: '+prevQuestion[2];

	if (userAnswer == correctAnswer) {
		score++;
		document.getElementById("pa1").style.color='green';
		document.getElementById("pa2").innerHTML='Correct!';
	} else {
		document.getElementById("pa1").style.color='red';
		document.getElementById("pa2").innerHTML='The correct answer was: '+correctAnswer;
	}
	document.getElementById("pa2").style.color='green';

	// Store answer for the report
	answeredQuestions[currentQuestionNum] = prevQuestion[0];
	answeredQuestionsAnswers[currentQuestionNum] = userAnswer;
	answeredQuestionsCorrectAns[currentQuestionNum] = correctAnswer;
	answeredQuestionsCitations[currentQuestionNum] = prevQuestion[2];
	
	// Store other stats for report
	var section = prevQuestion[2]; 
	var addQuestion = false;
	
	if( (section == 1) ){
		section1numQs++;
		if (userAnswer == correctAnswer) {
			section1numQCorrect++;
		}
	} else if( (section == 2) ){
		section2numQs++;
		if (userAnswer == correctAnswer) {
			section2numQCorrect++;
		}
	} else if( (section == 3) ){
		section3numQs++;
		if (userAnswer == correctAnswer) {
			section3numQCorrect++;
		}
	} else if( (section == 4) ){
		section4numQs++;
		if (userAnswer == correctAnswer) {
			section4numQCorrect++;
		}
	} else if( (section == 5) ){
		section5numQs++;
		if (userAnswer == correctAnswer) {
			section5numQCorrect++;
		}
	} else if( (section == 6) ){
		section6numQs++;
		if (userAnswer == correctAnswer) {
			section6numQCorrect++;
		}
	} else if( (section == 7) ){
		section7numQs++;
		if (userAnswer == correctAnswer) {
			section7numQCorrect++;
		}
	} else if( (section == 8) ){
		section8numQs++;
		if (userAnswer == correctAnswer) {
			section8numQCorrect++;
		}
	} else if( (section == 9) ){
		section9numQs++;
		if (userAnswer == correctAnswer) {
			section9numQCorrect++;
		}
	} else if( (section == 10) ){
		section10numQs++;
		if (userAnswer == correctAnswer) {
			section10numQCorrect++;
		}
	} else if( (section == 11) ){
		section11numQs++;
		if (userAnswer == correctAnswer) {
			section11numQCorrect++;
		}
	} else if( (section == 12) ){
		section12numQs++;
		if (userAnswer == correctAnswer) {
			section12numQCorrect++;
		}
	} else if( (section == 13) ){
		section13numQs++;
		if (userAnswer == correctAnswer) {
			section13numQCorrect++;
		}
	} else if( (section == 14) ){
		section14numQs++;
		if (userAnswer == correctAnswer) {
			section14numQCorrect++;
		}
	} else if( (section == 15) ){
		section15numQs++;
		if (userAnswer == correctAnswer) {
			section15numQCorrect++;
		}
	}

	// Reset the text field
        theForm.elements.givenAnswer.value = "";

	// Grab the new question
	currentQuestionNum++;
	
	var numQuestionsToAsk;
	numQuestionsToAsk = quizSize;

	if(currentQuestionNum >= numQuestionsToAsk){
		displayReport();
	} else {
		currentQuestion = quiz[currentQuestionNum].split('~');
		correctAnswer = currentQuestion[1];

		//adjustRadioButtons(currentQuestion);
		//randomizeAnswers(currentQuestion);

		// Display the new question
		document.getElementById("qn").innerHTML='Question #'+(currentQuestionNum+1)+" / "+quizSize;
		document.getElementById("s").innerHTML='Score: '+ score+'/'+currentQuestionNum+' ('+(Math.round(((score/currentQuestionNum)*100)*Math.pow(10,2))/Math.pow(10,2))+'%)';
		document.getElementById("q").innerHTML='Question: '+currentQuestion[0];
	}
}

function isNumber( o ) {
	return isNaN (o-0);
}

function getQuizParams(){
	displayQuizParams();
}

function startQuiz(){
	
	var i = 0;
	var numQuestionsToAsk;
	
	r = totalNumQuestions;
	currentQuestionNum = 0;
	score = 0;
	section1numQs = 0;
	section1numQCorrect = 0;
	section2numQs = 0;
	section2numQCorrect = 0;
	section3numQs = 0;
	section3numQCorrect = 0;
	section4numQs = 0;
	section4numQCorrect = 0;
	section5numQs = 0;
	section5numQCorrect = 0;
	section6numQs = 0;
	section6numQCorrect = 0;
	section7numQs = 0;
	section7numQCorrect = 0;
	section8numQs = 0;
	section8numQCorrect = 0;
	section9numQs = 0;
	section9numQCorrect = 0;
	section10numQs = 0;
	section10numQCorrect = 0;
	section11numQs = 0;
	section11numQCorrect = 0;
	section12numQs = 0;
	section12numQCorrect = 0;
	section13numQs = 0;
	section13numQCorrect = 0;
	section14numQs = 0;
	section14numQCorrect = 0;
	section15numQs = 0;
	section15numQCorrect = 0;
	document.getElementById("repQ").innerHTML=" ";

	// Reset all arrays (quiz, answeredQuestions, etc)
	quiz.length = 0;
	answeredQuestions.length = 0;
	answeredQuestionsAnswers.length = 0;
	answeredQuestionsCorrectAns.length = 0;
	answeredQuestionsCitations.length = 0;
	
	numQuestionsToAsk = document.getElementById('inoq').value;
	
	if( isNumber(numQuestionsToAsk) ) {
		numQuestionsToAsk = 1;
	}

	// Copy all questions from allQuestionsPool to questionsPool
	var questionsPool = new Array();
	var k;
	for(k = 0; k < totalNumQuestions; k++){
		questionsPool[k] = allQuestionsPool[k];
	}
	
	for ( ; (r >= 0) && (i < numQuestionsToAsk) && (questionsPool.length > 0); r--) {
		var randomnumber = Math.floor(Math.random() * r);
		
		currentQuestion = questionsPool[randomnumber].split('~');
		correctAnswer = currentQuestion[1];
		var section = currentQuestion[2];
		var addQuestion = false;
		
		if( (section == 1) && (	document.getElementById('is1').checked ) ){
			addQuestion = true;
		} else if( (section == 2) && (	document.getElementById('is2').checked ) ){
			addQuestion = true;
		} else if( (section == 3) && (	document.getElementById('is3').checked ) ){
			addQuestion = true;
		} else if( (section == 4) && (	document.getElementById('is4').checked ) ){
			addQuestion = true;
		} else if( (section == 5) && (	document.getElementById('is5').checked ) ){
			addQuestion = true;
		} else if( (section == 6) && (	document.getElementById('is6').checked ) ){
			addQuestion = true;
		} else if( (section == 7) && (	document.getElementById('is7').checked ) ){
			addQuestion = true;
		} else if( (section == 8) && (	document.getElementById('is8').checked ) ){
			addQuestion = true;
		} else if( (section == 9) && (	document.getElementById('is9').checked ) ){
			addQuestion = true;
		} else if( (section == 10) && (	document.getElementById('is10').checked ) ){
			addQuestion = true;
		} else if( (section == 11) && (	document.getElementById('is11').checked ) ){
			addQuestion = true;
		} else if( (section == 12) && (	document.getElementById('is12').checked ) ){
			addQuestion = true;
		} else if( (section == 13) && (	document.getElementById('is13').checked ) ){
			addQuestion = true;
		} else if( (section == 14) && (	document.getElementById('is14').checked ) ){
			addQuestion = true;
		} else if( (section == 15) && (	document.getElementById('is15').checked ) ){
			addQuestion = true;
		}
		
		if(addQuestion){
			quiz[i++] = questionsPool[randomnumber];
		}

		// Remove question from pool
		questionsPool.splice(randomnumber, 1);
	}

	quizSize = quiz.length;

	if (currentQuestionNum < quiz.length) {
		currentQuestion = quiz[currentQuestionNum].split('~');
		correctAnswer = currentQuestion[1];
	} else {
		alert("Something went wrong!");
	}

	// Hide quiz params form, show questions
	displayQuestions();

	//adjustRadioButtons();
	//randomizeAnswers();
	
	prevQuestion = [];
	for( var i=0; i<currentQuestion.length; i++){
		prevQuestion[i] = currentQuestion[i];
	}

	document.getElementById("qn").innerHTML='Question #'+(currentQuestionNum+1)+" / "+quizSize;
	document.getElementById("s").innerHTML='Score: '+score; // Should be 0
	document.getElementById("q").innerHTML='Question: '+currentQuestion[0];
/*
	document.getElementById('la1').firstChild.nodeValue=currentQuestion[1];
	document.getElementById('la2').firstChild.nodeValue=currentQuestion[2];
	document.getElementById('la3').firstChild.nodeValue=currentQuestion[3];
	document.getElementById('la4').firstChild.nodeValue=currentQuestion[4];
	document.getElementById('la5').firstChild.nodeValue=currentQuestion[5];
	document.getElementById('la6').firstChild.nodeValue=currentQuestion[6];
	document.getElementById('la7').firstChild.nodeValue=currentQuestion[7];
	document.getElementById('la8').firstChild.nodeValue=currentQuestion[8];
	document.getElementById('la9').firstChild.nodeValue=currentQuestion[9];
*/
}

function displayPrintableQuiz(){
	startQuiz();
}

function displayReport(){
	var adjustment = 2;
	var percentScore = (Math.round(((score/currentQuestionNum)*100)*Math.pow(10,2))/Math.pow(10,2));
	var greenScore = Math.round(percentScore/adjustment);
	var redScore = (100/adjustment) - greenScore;
	
	// Overall score
	document.getElementById("repOS").innerHTML=
		'Overall Score: '+percentScore+'%  ('+ score+' / '+currentQuestionNum+')</p>\n';

	document.getElementById("repOSg").innerHTML=' ';
	for(var i=0; i<greenScore; i++){
		document.getElementById("repOSg").innerHTML=document.getElementById("repOSg").innerHTML
			+'<font color=#00FF00>|';
	}
	
	document.getElementById("repOSr").innerHTML=' ';
	for(var i=0; i<redScore; i++){
		document.getElementById("repOSr").innerHTML=document.getElementById("repOSr").innerHTML
			+'<font color=#FF0000>|';
	}
	
	
	// Section scores
	if(section1numQs > 0){
		percentScore = (Math.round(((section1numQCorrect/section1numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS1").innerHTML=
			'Section 1: '+percentScore+'%  ('+ section1numQCorrect+' / '+section1numQs+')\n';

		document.getElementById("repS1g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS1g").innerHTML=document.getElementById("repS1g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS1r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS1r").innerHTML=document.getElementById("repS1r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS1").innerHTML='Section 1: NA\n';
		document.getElementById("repS1g").innerHTML=' ';
		document.getElementById("repS1r").innerHTML=' ';
	}

	if(section2numQs > 0){
		percentScore = (Math.round(((section2numQCorrect/section2numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS2").innerHTML=
			'Section 2: '+percentScore+'%  ('+ section2numQCorrect+' / '+section2numQs+')\n';

		document.getElementById("repS2g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS2g").innerHTML=document.getElementById("repS2g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS2r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS2r").innerHTML=document.getElementById("repS2r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS2").innerHTML='Section 2: NA \n';
		document.getElementById("repS2g").innerHTML=' ';
		document.getElementById("repS2r").innerHTML=' ';
	}

	if(section3numQs > 0){
		percentScore = (Math.round(((section3numQCorrect/section3numQs)*100)*Math.pow(10,2))/Math.pow(10,2)); 
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS3").innerHTML=
			'Section 3: '+percentScore+'%  ('+ section3numQCorrect+' / '+section3numQs+')\n';

		document.getElementById("repS3g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS3g").innerHTML=document.getElementById("repS3g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS3r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS3r").innerHTML=document.getElementById("repS3r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS3").innerHTML='Section 3: NA\n';
		document.getElementById("repS3g").innerHTML=' ';
		document.getElementById("repS3r").innerHTML=' ';
	}

	if(section4numQs > 0){
		percentScore = (Math.round(((section4numQCorrect/section4numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS4").innerHTML=
			'Section 4: '+percentScore+'%  ('+ section4numQCorrect+' / '+section4numQs+')\n';

		document.getElementById("repS4g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS4g").innerHTML=document.getElementById("repS4g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS4r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS4r").innerHTML=document.getElementById("repS4r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS4").innerHTML='Section 4: NA\n';
		document.getElementById("repS4g").innerHTML=' ';
		document.getElementById("repS4r").innerHTML=' ';
	}

	if(section5numQs > 0){
		percentScore = (Math.round(((section5numQCorrect/section5numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS5").innerHTML=
			'Section 5: '+percentScore+'%  ('+ section5numQCorrect+' / '+section5numQs+')\n';

		document.getElementById("repS5g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS5g").innerHTML=document.getElementById("repS5g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS5r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS5r").innerHTML=document.getElementById("repS5r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS5").innerHTML='Section 5: NA\n';
		document.getElementById("repS5g").innerHTML=' ';
		document.getElementById("repS5r").innerHTML=' ';
	}

	if(section6numQs > 0){
		percentScore = (Math.round(((section6numQCorrect/section6numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS6").innerHTML=
			'Section 6: '+percentScore+'%  ('+ section6numQCorrect+' / '+section6numQs+')\n';

		document.getElementById("repS6g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS6g").innerHTML=document.getElementById("repS6g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS6r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS6r").innerHTML=document.getElementById("repS6r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS6").innerHTML='Section 6: NA\n';
		document.getElementById("repS6g").innerHTML=' ';
		document.getElementById("repS6r").innerHTML=' ';
	}

	if(section7numQs > 0){
		percentScore = (Math.round(((section7numQCorrect/section7numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS7").innerHTML=
			'Section 7: '+percentScore+'%  ('+ section7numQCorrect+' / '+section7numQs+')\n';

		document.getElementById("repS7g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS7g").innerHTML=document.getElementById("repS7g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS7r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS7r").innerHTML=document.getElementById("repS7r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS7").innerHTML='Section 7: NA\n';
		document.getElementById("repS7g").innerHTML=' ';
		document.getElementById("repS7r").innerHTML=' ';
	}

	if(section8numQs > 0){
		percentScore = (Math.round(((section8numQCorrect/section8numQs)*100)*Math.pow(10,2))/Math.pow(10,2)); 
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS8").innerHTML=
			'Section 8: '+percentScore+'%  ('+ section8numQCorrect+' / '+section8numQs+')\n';

		document.getElementById("repS8g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS8g").innerHTML=document.getElementById("repS8g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS8r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS8r").innerHTML=document.getElementById("repS8r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS8").innerHTML='Section 8: NA\n';
		document.getElementById("repS8g").innerHTML=' ';
		document.getElementById("repS8r").innerHTML=' ';
	}

	if(section9numQs > 0){
		percentScore = (Math.round(((section9numQCorrect/section9numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS9").innerHTML=
			'Section 9: '+percentScore+'%  ('+ section9numQCorrect+' / '+section9numQs+')\n';

		document.getElementById("repS9g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS9g").innerHTML=document.getElementById("repS9g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS9r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS9r").innerHTML=document.getElementById("repS9r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS9").innerHTML='Section 9: NA\n';
		document.getElementById("repS9g").innerHTML=' ';
		document.getElementById("repS9r").innerHTML=' ';
	}

	if(section10numQs > 0){
		percentScore = (Math.round(((section10numQCorrect/section10numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS10").innerHTML=
			'Section 10: '+percentScore+'%  ('+ section10numQCorrect+' / '+section10numQs+')\n';

		document.getElementById("repS10g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS10g").innerHTML=document.getElementById("repS10g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS10r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS10r").innerHTML=document.getElementById("repS10r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS10").innerHTML='Section 10: NA\n';
		document.getElementById("repS10g").innerHTML=' ';
		document.getElementById("repS10r").innerHTML=' ';
	}
	
	if(section11numQs > 0){
		percentScore = (Math.round(((section11numQCorrect/section11numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS11").innerHTML=
			'Section 11: '+percentScore+'%  ('+ section11numQCorrect+' / '+section11numQs+')\n';

		document.getElementById("repS11g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS11g").innerHTML=document.getElementById("repS11g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS11r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS11r").innerHTML=document.getElementById("repS11r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS11").innerHTML='Section 11: NA\n';
		document.getElementById("repS11g").innerHTML=' ';
		document.getElementById("repS11r").innerHTML=' ';
	}
	

	if(section12numQs > 0){
		percentScore = (Math.round(((section12numQCorrect/section12numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS12").innerHTML=
			'Section 12: '+percentScore+'%  ('+ section12numQCorrect+' / '+section12numQs+')\n';

		document.getElementById("repS12g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS12g").innerHTML=document.getElementById("repS12g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS12r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS12r").innerHTML=document.getElementById("repS12r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS12").innerHTML='Section 12: NA\n';
		document.getElementById("repS12g").innerHTML=' ';
		document.getElementById("repS12r").innerHTML=' ';
	}
	

	if(section13numQs > 0){
		percentScore = (Math.round(((section13numQCorrect/section13numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS13").innerHTML=
			'Section 13: '+percentScore+'%  ('+ section13numQCorrect+' / '+section13numQs+')\n';

		document.getElementById("repS13g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS13g").innerHTML=document.getElementById("repS13g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS13r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS13r").innerHTML=document.getElementById("repS13r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS13").innerHTML='Section 13: NA\n';
		document.getElementById("repS13g").innerHTML=' ';
		document.getElementById("repS13r").innerHTML=' ';
	}
	

	if(section14numQs > 0){
		percentScore = (Math.round(((section14numQCorrect/section14numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS14").innerHTML=
			'Section 14: '+percentScore+'%  ('+ section14numQCorrect+' / '+section14numQs+')\n';

		document.getElementById("repS14g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS14g").innerHTML=document.getElementById("repS14g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS14r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS14r").innerHTML=document.getElementById("repS14r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS14").innerHTML='Section 14: NA\n';
		document.getElementById("repS14g").innerHTML=' ';
		document.getElementById("repS14r").innerHTML=' ';
	}
	

	if(section15numQs > 0){
		percentScore = (Math.round(((section15numQCorrect/section15numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS15").innerHTML=
			'Section 15: '+percentScore+'%  ('+ section15numQCorrect+' / '+section15numQs+')\n';

		document.getElementById("repS15g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS15g").innerHTML=document.getElementById("repS15g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS15r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS15r").innerHTML=document.getElementById("repS15r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS15").innerHTML='Section 15: NA\n';
		document.getElementById("repS15g").innerHTML=' ';
		document.getElementById("repS15r").innerHTML=' ';
	}
	
	// Questions
	for ( var i = 0; i < answeredQuestions.length; i++) {
		document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" * "+answeredQuestions[i]
			+" -- Your Answer: "+answeredQuestionsAnswers[i];
		
		if(answeredQuestionsAnswers[i] == answeredQuestionsCorrectAns[i]){
			document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" (Correct!)";
		} else {
			document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" (Wrong)"
			+" -- The Correct Answer: "+answeredQuestionsCorrectAns[i];
		}
		document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" -- Citation: "+answeredQuestionsCitations[i]
			+"</p>\n<p>";
	}
		
	document.getElementById('f').style.display = 'none'; //hide it
	document.getElementById('f2').style.display = 'none'; //hide it
	document.getElementById('f3').style.display = 'inline'; //show it
}

function displayQuizParams(){
	document.getElementById('f').style.display = 'none'; //hide it
	document.getElementById('f2').style.display = 'inline'; //show it
	document.getElementById('f3').style.display = 'none'; //hide it

	answeredQuestionsAnswers.length = 0;
	answeredQuestionsCorrectAns.length = 0;
	answeredQuestionsCitations.length = 0;
}

function displayQuestions(){
	document.getElementById('f').style.display = 'inline'; //show it
	document.getElementById('f2').style.display = 'none'; //hide it
	document.getElementById('f3').style.display = 'none'; //hide it
}

/**
 * Main logic starts here.
 */

// TODO: Loop through all questions to grab only those the user wants to be tested on.
// allQuestionsPool -> usableQuestions

// TODO: Randomize the remaining questions, only taking the number of questions the user wants.
// usableQuestions -> quiz

	
document.write("<form  name=\"myInputForm\" id=\"f2\" onsubmit=\"return false\"><br />\n");
document.write("<p id=\"inoql\">How many questions would you like to be asked?</p>\n");
document.write("\n");
	
document.write(" Number of Questions: <input type=\"text\" id=\"inoq\" value=\""+totalNumQuestions+"\" /><br />\n");
document.write(" <input type=\"checkbox\" id=\"is1\" value=\"1\" /> Section 1: old vocab<br />\n");
document.write(" <input type=\"checkbox\" id=\"is2\" value=\"2\" /> Section 2: Question / Specifying Words<br />\n");
document.write(" <input type=\"checkbox\" id=\"is3\" value=\"3\" /> Section 3: Locations & Modes of Transporation<br />\n");
document.write(" <input type=\"checkbox\" id=\"is4\" value=\"4\" /> Section 4: At the Office<br />\n");
document.write(" <input type=\"checkbox\" id=\"is5\" value=\"5\" /> Section 5: Time Expressions<br />\n");
document.write(" <input type=\"checkbox\" id=\"is6\" value=\"6\" /> Section 6: Prepositions and Relative Position<br />\n");
document.write(" <input type=\"checkbox\" id=\"is7\" value=\"7\" /> Section 7: Misc. Vocab<br />\n");
document.write(" <input type=\"checkbox\" id=\"is8\" value=\"8\" /> Section 8: Phrases / Greetings<br />\n");
document.write(" <input type=\"checkbox\" id=\"is9\" value=\"9\" /> Section 9: Food<br />\n");
document.write(" <input type=\"checkbox\" id=\"is10\" value=\"10\" /> Section 10: Money/Counting<br />\n");
document.write(" <input type=\"checkbox\" id=\"is11\" value=\"11\" /> Section 11: Adjectives<br />\n");
document.write(" <input type=\"checkbox\" id=\"is12\" value=\"12\" /> Section 12: Verbs<br />\n");
document.write(" <input type=\"checkbox\" id=\"is13\" value=\"13\" /> Section 13: Vacation and Sites<br />\n");
document.write(" <input type=\"checkbox\" id=\"is14\" value=\"14\" /> Section 14: Family<br />\n");
document.write(" <input type=\"checkbox\" id=\"is15\" value=\"15\" /> Section 15: Health and body parts<br />\n");

document.write(" <input type=\"button\" value=\"Start Online Quiz\" onclick=\"startQuiz()\" > ");
//document.write(" <input type=\"button\" value=\" \" onclick=\"displayPrintableQuiz()\" > \n");
document.write("</form>\n");

document.getElementById('is1').checked=false;
document.getElementById('is2').checked=true;
document.getElementById('is3').checked=true;
document.getElementById('is4').checked=true;
document.getElementById('is5').checked=true;
document.getElementById('is6').checked=true;
document.getElementById('is7').checked=true;
document.getElementById('is8').checked=true;
document.getElementById('is9').checked=true;
document.getElementById('is10').checked=true;
document.getElementById('is11').checked=true;
document.getElementById('is12').checked=true;
document.getElementById('is13').checked=true;
document.getElementById('is14').checked=true;
document.getElementById('is15').checked=true;

document.write("<form name=\"myForm\" id=\"f\"><br />\n");
document.write("<p id=\"qn\"> </p>\n");
document.write("<p id=\"s\"> </p>\n");
document.write("<p id=\"q\"> </p>\n");
//document.write(" <input type=\"text\" name=\"givenAnswer\" id=\"a1\" onkeyup=\"if (event.keyCode == 13) {document.getElementById(\'nextButton\').click()}\" /> \n");
document.write(" <input type=\"text\" name=\"givenAnswer\" id=\"a1\" onkeyup=\"if (event.keyCode == 13) document.getElementById(\'nextButton\').click()\" /> \n");
///////document.write(" <input type=\"submit\" name=\"givenAnswer\" id=\"a1\" /> \n");

//document.write(" <input type=\"button\" value=\"Next Question\" onclick=\"checkMCAnswer(myForm)\" > \n");
document.write(" <input type=\"button\" value=\"Next Question\" id=\"nextButton\" onclick=\"checkTextAnswer(myForm)\" > \n");
document.write(" <input type=\"button\" value=\"Progress Report\" onclick=\"displayReport()\" > \n");

document.write("<p id=\"pq\"> </p>");
document.write("<p id=\"pa1\"> </p> \n");
document.write("<p id=\"pa2\"> </p> \n");
document.write("<p id=\"pc\"> </p> \n");

document.write("</form>\n");

/*
<script>
  document.myform.onsubmit = function(){
    alert('handled');
    return false;
  }
</script>
*/


document.write("<form name=\"myRepForm\" id=\"f3\"><br />\n");
document.write("<p>== Your report ==</p>");
//document.write("<p id=\"repOS\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repOS\"> </td>");
document.write("<td id=\"repOSg\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repOSr\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");

document.write("<p>== Section Breakdown ==</p>");
//document.write("<p id=\"repS1\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS1\"> </td>");
document.write("<td id=\"repS1g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS1r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS2\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS2\"> </td>");
document.write("<td id=\"repS2g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS2r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS3\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS3\"> </td>");
document.write("<td id=\"repS3g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS3r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS4\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS4\"> </td>");
document.write("<td id=\"repS4g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS4r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS5\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS5\"> </td>");
document.write("<td id=\"repS5g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS5r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS6\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS6\"> </td>");
document.write("<td id=\"repS6g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS6r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS7\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS7\"> </td>");
document.write("<td id=\"repS7g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS7r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS8\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS8\"> </td>");
document.write("<td id=\"repS8g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS8r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS9\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS9\"> </td>");
document.write("<td id=\"repS9g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS9r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS10\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS10\"> </td>");
document.write("<td id=\"repS10g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS10r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS11\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS11\"> </td>");
document.write("<td id=\"repS11g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS11r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS12\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS12\"> </td>");
document.write("<td id=\"repS12g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS12r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS13\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS13\"> </td>");
document.write("<td id=\"repS13g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS13r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS14\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS14\"> </td>");
document.write("<td id=\"repS14g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS14r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS15\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS15\"> </td>");
document.write("<td id=\"repS15g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS15r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
document.write("<p>== Question Breakdown ==</p>");
document.write("<p id=\"repQ\"> </p>");
document.write(" <input type=\"button\" value=\"Continue Online Quiz\" onclick=\"displayQuestions()\" > ");
document.write(" <input type=\"button\" value=\"Restart Online Quiz\" onclick=\"displayQuizParams()\" > ");
document.write("</form>\n");

getQuizParams();

document.write('\n');

// EOF
