/**
 * Check the user's answer for the current question and load the next question.
 * 
 * @param theForm
 */
function checkAnswer(theForm) {
	var userAnswer = -1;
	
	// Get the user's answer
	for ( var i = 0; i < theForm.c.length; i++) {
		if (theForm.c[i].checked) {
			userAnswer = i;
			theForm.c[i].checked = false;
		}
	}
	
	// Make sure the user chose something
	if (userAnswer == -1) {
		alert("You must select an answer");
		return false;
	}
	
	// Clear previous question
	prevQuestion.splice(0, prevQuestion.length);
	
	// Copy current question into previous question
	for(var i=0; i<currentQuestion.length; i++){
		prevQuestion[i] = currentQuestion[i];
	}

	// Display the previous question info
	document.getElementById("pq").innerHTML='Previous Question: '+prevQuestion[0];
	document.getElementById("pa1").innerHTML='You answered: '+prevQuestion[userAnswer+1];
	document.getElementById("pc").innerHTML='Citation: '+prevQuestion[prevQuestion.length-1];
	
	if (userAnswer == correctAnswer) {
		score++;
		document.getElementById("pa1").style.color='green';
		document.getElementById("pa2").innerHTML='Correct!';
	} else {
		document.getElementById("pa1").style.color='red';
		document.getElementById("pa2").innerHTML='The correct answer was: '+prevQuestion[correctAnswer+1];
	}
	document.getElementById("pa2").style.color='green';

	// Store answer for the report
	answeredQuestions[currentQuestionNum] = prevQuestion[0];
	answeredQuestionsAnswers[currentQuestionNum] = prevQuestion[userAnswer+1];
	answeredQuestionsCorrectAns[currentQuestionNum] = prevQuestion[correctAnswer+1];
	answeredQuestionsCitations[currentQuestionNum] = prevQuestion[prevQuestion.length-1];
	
	// Store other stats for report
	var citation = prevQuestion[prevQuestion.length-1]; 
	var locStart = citation.indexOf("-") + 10;
	var locEnd = citation.indexOf(".");
	var section = citation.substring(locStart, locEnd);
	var addQuestion = false;
	
	if( (section == 1) ){
		section1numQs++;
		if (userAnswer == correctAnswer) {
			section1numQCorrect++;
		}
	} else if( (section == 2) ){
		section2numQs++;
		if (userAnswer == correctAnswer) {
			section2numQCorrect++;
		}
	} else if( (section == 3) ){
		section3numQs++;
		if (userAnswer == correctAnswer) {
			section3numQCorrect++;
		}
	} else if( (section == 4) ){
		section4numQs++;
		if (userAnswer == correctAnswer) {
			section4numQCorrect++;
		}
	} else if( (section == 5) ){
		section5numQs++;
		if (userAnswer == correctAnswer) {
			section5numQCorrect++;
		}
	} else if( (section == 6) ){
		section6numQs++;
		if (userAnswer == correctAnswer) {
			section6numQCorrect++;
		}
	} else if( (section == 7) ){
		section7numQs++;
		if (userAnswer == correctAnswer) {
			section7numQCorrect++;
		}
	} else if( (section == 8) ){
		section8numQs++;
		if (userAnswer == correctAnswer) {
			section8numQCorrect++;
		}
	} else if( (section == 9) ){
		section9numQs++;
		if (userAnswer == correctAnswer) {
			section9numQCorrect++;
		}
	} else if( (section == 10) ){
		section10numQs++;
		if (userAnswer == correctAnswer) {
			section10numQCorrect++;
		}
	}

	// Grab the new question
	currentQuestionNum++;
	
	var numQuestionsToAsk;
	numQuestionsToAsk = document.getElementById('inoq').value;

	if(currentQuestionNum >= numQuestionsToAsk){
		displayReport();
	} else {
		currentQuestion = quiz[currentQuestionNum].split('~');

		adjustRadioButtons(currentQuestion);
		randomizeAnswers(currentQuestion);

		// Display the new question
		document.getElementById("qn").innerHTML='Question #'+(currentQuestionNum+1)+" / "+quizSize;
		document.getElementById("s").innerHTML='Score: '+ score+'/'+currentQuestionNum+' ('+(Math.round(((score/currentQuestionNum)*100)*Math.pow(10,2))/Math.pow(10,2))+'%)';
		document.getElementById("q").innerHTML='Question: '+currentQuestion[0];
		document.getElementById('la1').firstChild.nodeValue=currentQuestion[1];
		document.getElementById('la2').firstChild.nodeValue=currentQuestion[2];
		document.getElementById('la3').firstChild.nodeValue=currentQuestion[3];
		document.getElementById('la4').firstChild.nodeValue=currentQuestion[4];
		document.getElementById('la5').firstChild.nodeValue=currentQuestion[5];
		document.getElementById('la6').firstChild.nodeValue=currentQuestion[6];
		document.getElementById('la7').firstChild.nodeValue=currentQuestion[7];
		document.getElementById('la8').firstChild.nodeValue=currentQuestion[8];
		document.getElementById('la9').firstChild.nodeValue=currentQuestion[9];
	}
}

/**
 * Adjust the radio buttons by making the appropriate radio buttons show/hide
 * based on if there is an answer to fill that radio button. Also deselect
 * all radio buttons.
 * 
 * @return
 */
function adjustRadioButtons(){
	var numberOfAnswers = currentQuestion.length - 2; //-1 for question, -1 for citation
	var i = 1;
	
	// Show/hide all radio buttons
	if (i <= numberOfAnswers){
		document.getElementById('la1').style.display = 'inline'; //show it
		document.getElementById('a1').style.display = 'inline'; //show it
	} else {
		document.getElementById('la1').style.display = 'none'; //hide it
		document.getElementById('a1').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la2').style.display = 'inline'; //show it
		document.getElementById('a2').style.display = 'inline'; //show it
	} else {
		document.getElementById('la2').style.display = 'none'; //hide it
		document.getElementById('a2').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la3').style.display = 'inline'; //show it
		document.getElementById('a3').style.display = 'inline'; //show it
	} else {
		document.getElementById('la3').style.display = 'none'; //hide it
		document.getElementById('a3').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la4').style.display = 'inline'; //show it
		document.getElementById('a4').style.display = 'inline'; //show it
	} else {
		document.getElementById('la4').style.display = 'none'; //hide it
		document.getElementById('a4').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la5').style.display = 'inline'; //show it
		document.getElementById('a5').style.display = 'inline'; //show it
	} else {
		document.getElementById('la5').style.display = 'none'; //hide it
		document.getElementById('a5').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la6').style.display = 'inline'; //show it
		document.getElementById('a6').style.display = 'inline'; //show it
	} else {
		document.getElementById('la6').style.display = 'none'; //hide it
		document.getElementById('a6').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la7').style.display = 'inline'; //show it
		document.getElementById('a7').style.display = 'inline'; //show it
	} else {
		document.getElementById('la7').style.display = 'none'; //hide it
		document.getElementById('a7').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la8').style.display = 'inline'; //show it
		document.getElementById('a8').style.display = 'inline'; //show it
	} else {
		document.getElementById('la8').style.display = 'none'; //hide it
		document.getElementById('a8').style.display = 'none'; //hide it
	}
	i++;
	if (i <= numberOfAnswers){
		document.getElementById('la9').style.display = 'inline'; //show it
		document.getElementById('a9').style.display = 'inline'; //show it
	} else {
		document.getElementById('la9').style.display = 'none'; //hide it
		document.getElementById('a9').style.display = 'none'; //hide it
	}
	
	// Reset radio button choice
	var radList = document.getElementsByName('c');
	for (var i = 0; i < radList.length; i++) {
	}
}

/**
 * Make the order of the answers appear in a random order. This prevents users
 * from memorizing a particular answer (3rd choice for example) as opposed to
 * the actual answer.
 */
function randomizeAnswers(){
	
	var finalAnswers = new Array();
	var answersLength = currentQuestion.length-2;
	var assignedCorrectAnswer = false;
	var citation = currentQuestion[currentQuestion.length-1];
	
	// Randomly copy answers into finalAnswers
	for(var i=0; i<answersLength; i++){
		
		var randomNumber = (Math.floor(Math.random() * (currentQuestion.length-2))) + 1; // -2 to avoid citation and question, +1 shift

		finalAnswers[i] = currentQuestion[randomNumber];
		
		if(randomNumber == 1 && assignedCorrectAnswer == false){
			correctAnswer = i;
			assignedCorrectAnswer = true;
		}
		
		currentQuestion.splice(randomNumber,1);
	}
	
	// Copy back into currentQuestion
	for(var i=0; i<answersLength; i++){
		currentQuestion[i+1] = finalAnswers[i];
	}
	currentQuestion.push(citation);
}

function isNumber( o ) {
	return isNaN (o-0);
}

function getQuizParams(){
	displayQuizParams();
}

function startQuiz(){
	
	var i = 0;
	var numQuestionsToAsk;
	
	r = totalNumQuestions;
	currentQuestionNum = 0;
	score = 0;
	section1numQs = 0;
	section1numQCorrect = 0;
	section2numQs = 0;
	section2numQCorrect = 0;
	section3numQs = 0;
	section3numQCorrect = 0;
	section4numQs = 0;
	section4numQCorrect = 0;
	section5numQs = 0;
	section5numQCorrect = 0;
	section6numQs = 0;
	section6numQCorrect = 0;
	section7numQs = 0;
	section7numQCorrect = 0;
	section8numQs = 0;
	section8numQCorrect = 0;
	section9numQs = 0;
	section9numQCorrect = 0;
	section10numQs = 0;
	section10numQCorrect = 0;
	document.getElementById("repQ").innerHTML=" ";
	// TODO: Reset all arrays (quiz, answeredQuestions, etc)
	
	numQuestionsToAsk = document.getElementById('inoq').value;
	
	if( isNumber(numQuestionsToAsk) ) {
		numQuestionsToAsk = 50;
	}
	
	// Copy all questions from allQuestionsPool to questionsPool
	var questionsPool = new Array();
	var k;
	for(k = 0; k < totalNumQuestions; k++){
		questionsPool[k] = allQuestionsPool[k];
	}
	
	for ( ; (r >= 0) && (i < numQuestionsToAsk) ; r--) {
		var randomnumber = Math.floor(Math.random() * r);
		
		currentQuestion = questionsPool[randomnumber].split('~');
		var citation = currentQuestion[currentQuestion.length-1]; 
		var locStart = citation.indexOf("-") + 10;
		var locEnd = citation.indexOf(".");
		var section = citation.substring(locStart, locEnd);
		var addQuestion = false;
		
		if( (section == 1) && (	document.getElementById('is1').checked ) ){
			addQuestion = true;
		} else if( (section == 2) && (	document.getElementById('is2').checked ) ){
			addQuestion = true;
		} else if( (section == 3) && (	document.getElementById('is3').checked ) ){
			addQuestion = true;
		} else if( (section == 4) && (	document.getElementById('is4').checked ) ){
			addQuestion = true;
		} else if( (section == 5) && (	document.getElementById('is5').checked ) ){
			addQuestion = true;
		} else if( (section == 6) && (	document.getElementById('is6').checked ) ){
			addQuestion = true;
		} else if( (section == 7) && (	document.getElementById('is7').checked ) ){
			addQuestion = true;
		} else if( (section == 8) && (	document.getElementById('is8').checked ) ){
			addQuestion = true;
		} else if( (section == 9) && (	document.getElementById('is9').checked ) ){
			addQuestion = true;
		} else if( (section == 10) && (	document.getElementById('is10').checked ) ){
			addQuestion = true;
		}
		
		if(addQuestion){
			quiz[i++] = questionsPool[randomnumber];
		}
		
		// TODO remove question from pool
		
	}

	quizSize = quiz.length;

	if (currentQuestionNum < quiz.length) {
		currentQuestion = quiz[currentQuestionNum].split('~');
	} else {
		alert("Something went wrong!");
	}

	// Hide quiz params form, show questions
	displayQuestions();

	adjustRadioButtons();
	randomizeAnswers();
	
	prevQuestion = [];
	for( var i=0; i<currentQuestion.length; i++){
		prevQuestion[i] = currentQuestion[i];
	}

	document.getElementById("qn").innerHTML='Question #'+(currentQuestionNum+1)+" / "+quizSize;
	document.getElementById("s").innerHTML='Score: '+score; // Should be 0
	document.getElementById("q").innerHTML='Question: '+currentQuestion[0];
	document.getElementById('la1').firstChild.nodeValue=currentQuestion[1];
	document.getElementById('la2').firstChild.nodeValue=currentQuestion[2];
	document.getElementById('la3').firstChild.nodeValue=currentQuestion[3];
	document.getElementById('la4').firstChild.nodeValue=currentQuestion[4];
	document.getElementById('la5').firstChild.nodeValue=currentQuestion[5];
	document.getElementById('la6').firstChild.nodeValue=currentQuestion[6];
	document.getElementById('la7').firstChild.nodeValue=currentQuestion[7];
	document.getElementById('la8').firstChild.nodeValue=currentQuestion[8];
	document.getElementById('la9').firstChild.nodeValue=currentQuestion[9];

}

function displayPrintableQuiz(){
	startQuiz();
}

function displayReport(){
	var adjustment = 2;
	var percentScore = (Math.round(((score/currentQuestionNum)*100)*Math.pow(10,2))/Math.pow(10,2));
	var greenScore = Math.round(percentScore/adjustment);
	var redScore = (100/adjustment) - greenScore;
	
	// Overall score
	document.getElementById("repOS").innerHTML=
		'Overall Score: '+percentScore+'%  ('+ score+' / '+currentQuestionNum+')</p>\n';

	document.getElementById("repOSg").innerHTML=' ';
	for(var i=0; i<greenScore; i++){
		document.getElementById("repOSg").innerHTML=document.getElementById("repOSg").innerHTML
			+'<font color=#00FF00>|';
	}
	
	document.getElementById("repOSr").innerHTML=' ';
	for(var i=0; i<redScore; i++){
		document.getElementById("repOSr").innerHTML=document.getElementById("repOSr").innerHTML
			+'<font color=#FF0000>|';
	}
	
	
	// Section scores
	if(section1numQs > 0){
		percentScore = (Math.round(((section1numQCorrect/section1numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS1").innerHTML=
			'Section 1: '+percentScore+'%  ('+ section1numQCorrect+' / '+section1numQs+')\n';

		document.getElementById("repS1g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS1g").innerHTML=document.getElementById("repS1g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS1r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS1r").innerHTML=document.getElementById("repS1r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS1").innerHTML='Section 1: NA\n';
		document.getElementById("repS1g").innerHTML=' ';
		document.getElementById("repS1r").innerHTML=' ';
	}

	if(section2numQs > 0){
		percentScore = (Math.round(((section2numQCorrect/section2numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS2").innerHTML=
			'Section 2: '+percentScore+'%  ('+ section2numQCorrect+' / '+section2numQs+')\n';

		document.getElementById("repS2g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS2g").innerHTML=document.getElementById("repS2g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS2r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS2r").innerHTML=document.getElementById("repS2r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS2").innerHTML='Section 2: NA \n';
		document.getElementById("repS2g").innerHTML=' ';
		document.getElementById("repS2r").innerHTML=' ';
	}

	if(section3numQs > 0){
		percentScore = (Math.round(((section3numQCorrect/section3numQs)*100)*Math.pow(10,2))/Math.pow(10,2)); 
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS3").innerHTML=
			'Section 3: '+percentScore+'%  ('+ section3numQCorrect+' / '+section3numQs+')\n';

		document.getElementById("repS3g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS3g").innerHTML=document.getElementById("repS3g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS3r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS3r").innerHTML=document.getElementById("repS3r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS3").innerHTML='Section 3: NA\n';
		document.getElementById("repS3g").innerHTML=' ';
		document.getElementById("repS3r").innerHTML=' ';
	}

	if(section4numQs > 0){
		percentScore = (Math.round(((section4numQCorrect/section4numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS4").innerHTML=
			'Section 4: '+percentScore+'%  ('+ section4numQCorrect+' / '+section4numQs+')\n';

		document.getElementById("repS4g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS4g").innerHTML=document.getElementById("repS4g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS4r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS4r").innerHTML=document.getElementById("repS4r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS4").innerHTML='Section 4: NA\n';
		document.getElementById("repS4g").innerHTML=' ';
		document.getElementById("repS4r").innerHTML=' ';
	}

	if(section5numQs > 0){
		percentScore = (Math.round(((section5numQCorrect/section5numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS5").innerHTML=
			'Section 5: '+percentScore+'%  ('+ section5numQCorrect+' / '+section5numQs+')\n';

		document.getElementById("repS5g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS5g").innerHTML=document.getElementById("repS5g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS5r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS5r").innerHTML=document.getElementById("repS5r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS5").innerHTML='Section 5: NA\n';
		document.getElementById("repS5g").innerHTML=' ';
		document.getElementById("repS5r").innerHTML=' ';
	}

	if(section6numQs > 0){
		percentScore = (Math.round(((section6numQCorrect/section6numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS6").innerHTML=
			'Section 6: '+percentScore+'%  ('+ section6numQCorrect+' / '+section6numQs+')\n';

		document.getElementById("repS6g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS6g").innerHTML=document.getElementById("repS6g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS6r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS6r").innerHTML=document.getElementById("repS6r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS6").innerHTML='Section 6: NA\n';
		document.getElementById("repS6g").innerHTML=' ';
		document.getElementById("repS6r").innerHTML=' ';
	}

	if(section7numQs > 0){
		percentScore = (Math.round(((section7numQCorrect/section7numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS7").innerHTML=
			'Section 7: '+percentScore+'%  ('+ section7numQCorrect+' / '+section7numQs+')\n';

		document.getElementById("repS7g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS7g").innerHTML=document.getElementById("repS7g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS7r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS7r").innerHTML=document.getElementById("repS7r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS7").innerHTML='Section 7: NA\n';
		document.getElementById("repS7g").innerHTML=' ';
		document.getElementById("repS7r").innerHTML=' ';
	}

	if(section8numQs > 0){
		percentScore = (Math.round(((section8numQCorrect/section8numQs)*100)*Math.pow(10,2))/Math.pow(10,2)); 
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
		
		document.getElementById("repS8").innerHTML=
			'Section 8: '+percentScore+'%  ('+ section8numQCorrect+' / '+section8numQs+')\n';

		document.getElementById("repS8g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS8g").innerHTML=document.getElementById("repS8g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS8r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS8r").innerHTML=document.getElementById("repS8r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS8").innerHTML='Section 8: NA\n';
		document.getElementById("repS8g").innerHTML=' ';
		document.getElementById("repS8r").innerHTML=' ';
	}

	if(section9numQs > 0){
		percentScore = (Math.round(((section9numQCorrect/section9numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS9").innerHTML=
			'Section 9: '+percentScore+'%  ('+ section9numQCorrect+' / '+section9numQs+')\n';

		document.getElementById("repS9g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS9g").innerHTML=document.getElementById("repS9g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS9r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS9r").innerHTML=document.getElementById("repS9r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS9").innerHTML='Section 9: NA\n';
		document.getElementById("repS9g").innerHTML=' ';
		document.getElementById("repS9r").innerHTML=' ';
	}

	if(section10numQs > 0){
		percentScore = (Math.round(((section10numQCorrect/section10numQs)*100)*Math.pow(10,2))/Math.pow(10,2));
		greenScore = Math.round(percentScore / adjustment);
		redScore = (100 / adjustment) - greenScore;
			
		document.getElementById("repS10").innerHTML=
			'Section 10: '+percentScore+'%  ('+ section10numQCorrect+' / '+section10numQs+')\n';

		document.getElementById("repS10g").innerHTML=' ';
		for(var i=0; i<greenScore; i++){
			document.getElementById("repS10g").innerHTML=document.getElementById("repS10g").innerHTML
				+'<font color=#00FF00>|';
		}
		
		document.getElementById("repS10r").innerHTML=' ';
		for(var i=0; i<redScore; i++){
			document.getElementById("repS10r").innerHTML=document.getElementById("repS10r").innerHTML
				+'<font color=#FF0000>|';
		}
	} else {
		document.getElementById("repS10").innerHTML='Section 10: NA\n';
		document.getElementById("repS10g").innerHTML=' ';
		document.getElementById("repS10r").innerHTML=' ';
	}
	
	
	// Questions
	for ( var i = 0; i < answeredQuestions.length; i++) {
		document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" * "+answeredQuestions[i]
			+" -- Your Answer: "+answeredQuestionsAnswers[i];
		
		if(answeredQuestionsAnswers[i] == answeredQuestionsCorrectAns[i]){
			document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" (Correct!)";
		} else {
			document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" (Wrong)"
			+" -- The Correct Answer: "+answeredQuestionsCorrectAns[i];
		}
		document.getElementById("repQ").innerHTML=document.getElementById("repQ").innerHTML
			+" -- Citation: "+answeredQuestionsCitations[i]
			+"</p>\n<p>";
	}
		
	document.getElementById('f').style.display = 'none'; //hide it
	document.getElementById('f2').style.display = 'none'; //hide it
	document.getElementById('f3').style.display = 'inline'; //show it
}

function displayQuizParams(){
	document.getElementById('f').style.display = 'none'; //hide it
	document.getElementById('f2').style.display = 'inline'; //show it
	document.getElementById('f3').style.display = 'none'; //hide it
}

function displayQuestions(){
	document.getElementById('f').style.display = 'inline'; //show it
	document.getElementById('f2').style.display = 'none'; //hide it
	document.getElementById('f3').style.display = 'none'; //hide it
}

/**
 * Main logic starts here.
 */

// TODO: Loop through all questions to grab only those the user wants to be tested on.
// allQuestionsPool -> usableQuestions

// TODO: Randomize the remaining questions, only taking the number of questions the user wants.
// usableQuestions -> quiz

	
document.write("<form  name=\"myInputForm\" id=\"f2\"><br />\n");
document.write("<p id=\"inoql\">How many questions would you like to be asked?</p>\n");
document.write("\n");
	
//document.write(" Number of Questions: <input type=\"text\" name=\"inoq\" value=\""+totalNumQuestions+"\" /><br />\n");
document.write(" Number of Questions: <input type=\"text\" id=\"inoq\" value=\""+totalNumQuestions+"\" /><br />\n");
document.write(" <input type=\"checkbox\" id=\"is1\" value=\"1\" /> Section 1: Teams<br />\n");
document.write(" <input type=\"checkbox\" id=\"is2\" value=\"2\" /> Section 2: Game Parameters<br />\n");
document.write(" <input type=\"checkbox\" id=\"is3\" value=\"3\" /> Section 3: Player<br />\n");
document.write(" <input type=\"checkbox\" id=\"is4\" value=\"4\" /> Section 4: The Pack<br />\n");
document.write(" <input type=\"checkbox\" id=\"is5\" value=\"5\" /> Section 5: Blocking<br />\n");
document.write(" <input type=\"checkbox\" id=\"is6\" value=\"6\" /> Section 6: Penalties<br />\n");
document.write(" <input type=\"checkbox\" id=\"is7\" value=\"7\" /> Section 7: Penalty Enforcement<br />\n");
document.write(" <input type=\"checkbox\" id=\"is8\" value=\"8\" /> Section 8: Scoring<br />\n");
document.write(" <input type=\"checkbox\" id=\"is9\" value=\"9\" /> Section 9: Officials<br />\n");
document.write(" <input type=\"checkbox\" id=\"is10\" value=\"10\" /> Section 10: Safety<br />\n");
document.write(" <input type=\"button\" value=\"Start Online Quiz\" onclick=\"startQuiz()\" > ");
document.write(" <input type=\"button\" value=\" \" onclick=\"displayPrintableQuiz()\" > \n");
document.write("</form>\n");

document.getElementById('is1').checked=true;
document.getElementById('is2').checked=true;
document.getElementById('is3').checked=true;
document.getElementById('is4').checked=true;
document.getElementById('is5').checked=true;
document.getElementById('is6').checked=true;
document.getElementById('is7').checked=true;
document.getElementById('is8').checked=true;
document.getElementById('is9').checked=true;
document.getElementById('is10').checked=true;

document.write("<form name=\"myForm\" id=\"f\"><br />\n");
document.write("<p id=\"qn\"> </p>\n");
document.write("<p id=\"s\"> </p>\n");
document.write("<p id=\"q\"> </p>\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"1\" id=\"a1\" /> \n");
document.write(" <label for=\"a1\" id=\"la1\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"2\" id=\"a2\" /> \n");
document.write(" <label for=\"a2\" id=\"la2\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"3\" id=\"a3\" /> \n");
document.write(" <label for=\"a3\" id=\"la3\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"4\" id=\"a4\" /> \n");
document.write(" <label for=\"a4\" id=\"la4\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"5\" id=\"a5\" /> \n");
document.write(" <label for=\"a5\" id=\"la5\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"6\" id=\"a6\" /> \n");
document.write(" <label for=\"a6\" id=\"la6\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"7\" id=\"a7\" /> \n");
document.write(" <label for=\"a7\" id=\"la7\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"8\" id=\"a8\" /> \n");
document.write(" <label for=\"a8\" id=\"la8\"> </label><br />\n");
document.write(" <input type=\"radio\" name=\"c\" value=\"9\" id=\"a9\" /> \n");
document.write(" <label for=\"a9\" id=\"la9\"> </label><br />\n");

document.write(" <input type=\"button\" value=\"Next Question\" onclick=\"checkAnswer(myForm)\" > \n");
document.write(" <input type=\"button\" value=\"Progress Report\" onclick=\"displayReport()\" > \n");

document.write("<p id=\"pq\"> </p>");
document.write("<p id=\"pa1\"> </p> \n");
document.write("<p id=\"pa2\"> </p> \n");
document.write("<p id=\"pc\"> </p> \n");

document.write("</form>\n");

document.write("<form name=\"myRepForm\" id=\"f3\"><br />\n");
document.write("<p>== Your report ==</p>");
//document.write("<p id=\"repOS\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repOS\"> </td>");
document.write("<td id=\"repOSg\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repOSr\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");

document.write("<p>== Section Breakdown ==</p>");
//document.write("<p id=\"repS1\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS1\"> </td>");
document.write("<td id=\"repS1g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS1r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS2\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS2\"> </td>");
document.write("<td id=\"repS2g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS2r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS3\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS3\"> </td>");
document.write("<td id=\"repS3g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS3r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS4\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS4\"> </td>");
document.write("<td id=\"repS4g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS4r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS5\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS5\"> </td>");
document.write("<td id=\"repS5g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS5r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS6\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS6\"> </td>");
document.write("<td id=\"repS6g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS6r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS7\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS7\"> </td>");
document.write("<td id=\"repS7g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS7r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS8\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS8\"> </td>");
document.write("<td id=\"repS8g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS8r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS9\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS9\"> </td>");
document.write("<td id=\"repS9g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS9r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
//document.write("<p id=\"repS10\"> </p>");
document.write("<table border=\"0\">");
document.write("<tr>");
document.write("<td id=\"repS10\"> </td>");
document.write("<td id=\"repS10g\" bgcolor=#00FF00><font color=#00FF00>|</td>");
document.write("<td id=\"repS10r\" bgcolor=#FF0000><font color=#FF0000>|</td>");
document.write("</tr>");
document.write("</table> ");
document.write("<p>== Question Breakdown ==</p>");
document.write("<p id=\"repQ\"> </p>");
document.write(" <input type=\"button\" value=\"Continue Online Quiz\" onclick=\"displayQuestions()\" > ");
document.write(" <input type=\"button\" value=\"Restart Online Quiz\" onclick=\"displayQuizParams()\" > ");
document.write("</form>\n");

getQuizParams();

document.write('\n');

// EOF
